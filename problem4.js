import {inventory} from './inventory.js';

function carYears(inventory){
    let cars_year = [];
    for(let i=0;i<inventory.length;i++){
        cars_year.push(inventory[i].car_year);
    }
    return cars_year;
}

export var cars_year = carYears(inventory);
console.log(cars_year);
// export {carYears};