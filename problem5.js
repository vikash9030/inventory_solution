import { cars_year } from "./problem4.js";

function olderCars(cars_year){
    let older_cars = [];
    for(let i=0;i<cars_year.length;i++){
        if(cars_year[i] < 2000){
            older_cars.push(cars_year[i]);
        }
    }
    return older_cars;
}

let older_cars = olderCars(cars_year);
console.log(older_cars);