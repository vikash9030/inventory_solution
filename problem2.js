import { inventory } from './inventory.js';

function lastCar(inventory){
    let last_element = inventory.length - 1;
    return inventory[last_element];
}

let car = lastCar(inventory);
console.log(`Last car is a ${car.car_make} ${car.car_model}`)