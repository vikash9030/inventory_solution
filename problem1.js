import {inventory}  from './inventory.js';

function problem1(inventory){
    const car_id = 33;
    for(let i=0;i<inventory.length;i++){
        if(inventory[i].id === car_id){
            return inventory[i];
        }
    }
}
const car = problem1(inventory)
console.log(`Car 33 is a ${car.car_year} ${car.car_make} ${car.car_model}`);