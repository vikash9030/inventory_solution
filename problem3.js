import {inventory} from './inventory.js';

function carModels(inventory){
    let car_models = [];
    for(let i=0;i<inventory.length;i++){
        car_models.push(inventory[i].car_model);
    }
    return car_models;
}

var car_models = carModels(inventory);
car_models.sort();
console.log(car_models);