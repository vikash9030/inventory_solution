import {inventory} from './inventory.js';

function specificCars(inventory){
    let cars = [];
    for(let i=0;i<inventory.length;i++){
        // console.log(inventory[i].car_make);
        if(inventory[i].car_make == "BMW" || inventory[i].car_make == "Audi")
        cars.push(inventory[i]);
    }
    return cars;
}
var cars = specificCars(inventory);
console.log(cars);